package com.example.movieproject.mappers;


import com.example.movieproject.Dto.MoviecharacterDto;
import com.example.movieproject.models.Moviecharacter;
import com.example.movieproject.models.Movies;
import com.example.movieproject.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public abstract class MovieCharacterMapper {


        @Autowired
        protected MovieService movieService;

        @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
        public abstract MoviecharacterDto movieCharacterToMoviecharacterDto(Moviecharacter movieCharacter);


        public abstract Collection<MoviecharacterDto> movieCharacterToMoviecharacterDto(Collection<Moviecharacter> movieCharacter);


        @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesIdToMovies")
        public abstract Moviecharacter movieCharacterDtoToMovieCharacter(MoviecharacterDto dto);



        @Named("moviesIdToMovies")
        Set<Movies> mapIdsToMovies(Set<Integer> id) {
            return id.stream()
                    .map( i -> movieService.findById(i))
                    .collect(Collectors.toSet());
        }

    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movies> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }




}
