package com.example.movieproject.mappers;





import com.example.movieproject.Dto.MovieDto;
import com.example.movieproject.models.Franchises;
import com.example.movieproject.models.Moviecharacter;
import com.example.movieproject.models.Movies;
import com.example.movieproject.services.character.MovieCharacterService;
import com.example.movieproject.services.franchise.FranchisesService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    protected MovieCharacterService movieCharacterService;

    @Autowired
    protected FranchisesService franchisesService;

    @Mapping(target = "moviecharacter", source = "moviecharacter", qualifiedByName = "moviecharacterToIds")
    @Mapping(target = "franchises", source = "franchises.id")
    public abstract MovieDto moviesToMovieDto(Movies movies);


    public abstract Collection<MovieDto> moviesToMovieDto(Collection<Movies> movies);

    @Mapping(target = "franchises", source = "franchises", qualifiedByName = "franchisesIdTofranchises")
    @Mapping(target = "moviecharacter", source = "moviecharacter", qualifiedByName = "moviecharacterIdToMoviecharacter")
    public abstract Movies movieDtoToMovies(MovieDto dto);


    @Named("franchisesIdTofranchises")
    Franchises mapIdToFranchises(int id) {
        return franchisesService.findById(id);
    }

    @Named("moviecharacterIdToMoviecharacter")
    Set<Moviecharacter> mapIdsToMovieCharacter(Set<Integer> id) {
        return id.stream()
                .map( i -> movieCharacterService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("moviecharacterToIds")
    Set<Integer> mapmoviecharacterToIds(Set<Moviecharacter> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }


}
