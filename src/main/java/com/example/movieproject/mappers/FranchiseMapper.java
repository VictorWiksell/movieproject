package com.example.movieproject.mappers;





import com.example.movieproject.Dto.FranchiseDto;
import com.example.movieproject.models.Franchises;
import com.example.movieproject.models.Movies;
import com.example.movieproject.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {


        @Autowired
        protected MovieService movieServ;

        @Mapping(target = "movies", source = "movies", qualifiedByName = "movieToIds")
        public abstract FranchiseDto FranchisesToFranchiseDto(Franchises franchises);


        public abstract Collection<FranchiseDto> FranchisesToFranchiseDto(Collection<Franchises> franchises);


        @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovies")
        public abstract Franchises FranchiseDtoToFranchises(FranchiseDto dto);


       @Named("movieIdToMovies")
        Set<Movies> mapIdsToMovies(Set<Integer> id) {
            return id.stream()
                    .map( i -> movieServ.findById(i))
                    .collect(Collectors.toSet());
        }

        @Named("movieToIds")
        Set<Integer> mapMoviesToIds(Set<Movies> source) {
            if(source == null)
                return null;
            return source.stream()
                    .map(s -> s.getId()).collect(Collectors.toSet());
        }
}
