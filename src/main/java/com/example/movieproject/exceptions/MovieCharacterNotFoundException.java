package com.example.movieproject.exceptions;

import com.example.movieproject.models.Moviecharacter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class MovieCharacterNotFoundException extends RuntimeException{
        public MovieCharacterNotFoundException(int id) {
            super("Character does not exist with ID " + id);
        }
    }
