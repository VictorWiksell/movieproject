package com.example.movieproject.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;


@Getter
@Setter
@Entity
public class Moviecharacter {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private int id;

    private String fullName;
    private String alias;
    private String gender;
    private String profilePic;




    @ManyToMany(mappedBy = "moviecharacter")
    @JsonIgnore
    private Set<Movies> movies;



}
