package com.example.movieproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Set;



@Getter
@Setter
@Entity
public class Movies {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private int id;

    private String title;
    private String genre;
    private String releaseYear;
    private String director;
    private String picture;
    private String trailer;


    @ManyToMany
    @JoinTable(name = "moviecharacter_movies",
            joinColumns = {@JoinColumn(name = "movies_id")},
            inverseJoinColumns = {@JoinColumn(name = "moviecharacter_id")})
    @JsonIgnore
    private Set<Moviecharacter> moviecharacter;

    @ManyToOne
    @JoinColumn(name = "franchises_id")
    private Franchises franchises;


    public void addCharacter(Moviecharacter movChar){
        moviecharacter.add(movChar);
    }


}
