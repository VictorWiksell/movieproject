
package com.example.movieproject.repo;


import com.example.movieproject.models.Movies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface MovieRepo extends JpaRepository<Movies, Integer> {


    @Query("select m from Movies m where m.franchises.id = ?1")
    Collection<Movies>findMoviesByFranchisesId(int id);


}

