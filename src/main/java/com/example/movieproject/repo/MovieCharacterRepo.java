package com.example.movieproject.repo;

import com.example.movieproject.models.Moviecharacter;
import com.example.movieproject.models.Movies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;


@Repository
public interface MovieCharacterRepo extends JpaRepository<Moviecharacter, Integer> {
/*  *//*  @Query("SELECT c.fullName, c.alias, c.gender, c.profilePic, m.title, m.genre, m.picture, m.releaseYear \n" +
            "FROM MovieCharacter c\n" +
            "INNER JOIN LinkCharacterMovies AS l ON l.characterId=c.id\n" +
            "INNER JOIN Movies AS m ON m.id=l.movieId")
    Set<MovieCharacter> findMovieCharacterByMovies(String title);*//*

    @Query("SELECT mc FROM MovieCharacter mc")
    Set<MovieCharacter> getAllCharacters();


    @Query("SELECT mc FROM MovieCharacter mc WHERE mc.id=?1")
    Set<MovieCharacter> findCharacterById(int id);
*/
 /*   @Query("SELECT fullName FROM MovieCharacter ")
    String findCharacterByName(String name);*/

    @Query("SELECT mc from Moviecharacter mc join fetch mc.movies m join fetch m.franchises f where f.id = ?1")
    Collection<Moviecharacter> findAllCharactersFromFranchises(int id);

    @Query("Select c FROM Moviecharacter c JOIN FETCH c.movies m WHERE m.id=?1")
    Collection<Moviecharacter> findMoviecharacterByMovieId(int id);
}

