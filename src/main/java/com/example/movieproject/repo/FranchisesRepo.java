
package com.example.movieproject.repo;


import com.example.movieproject.models.Franchises;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FranchisesRepo extends JpaRepository<Franchises, Integer> {


}

