package com.example.movieproject.services.franchise;


import com.example.movieproject.models.Franchises;
import com.example.movieproject.services.CrudService;

public interface FranchisesService extends CrudService<Franchises, Integer> {
}
