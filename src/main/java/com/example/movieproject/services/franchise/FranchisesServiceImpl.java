package com.example.movieproject.services.franchise;


import com.example.movieproject.exceptions.FranchiseNotFoundException;
import com.example.movieproject.exceptions.MovieCharacterNotFoundException;
import com.example.movieproject.models.Franchises;
import com.example.movieproject.repo.FranchisesRepo;
import org.springframework.stereotype.Service;

import java.util.Collection;



@Service
public class FranchisesServiceImpl implements FranchisesService {

    private final FranchisesRepo franchisesRepo;

    public FranchisesServiceImpl(FranchisesRepo franchisesRepo) {
        this.franchisesRepo = franchisesRepo;
    }

    @Override
    public Franchises findById(Integer id) throws FranchiseNotFoundException {

        return franchisesRepo.findById(id).orElse(null);
    }

    @Override
    public Collection<Franchises> findAll() {
        return franchisesRepo.findAll();
    }

    @Override
    public Franchises add(Franchises entity) {
        return franchisesRepo.save(entity);
    }

    @Override
    public Franchises update(Franchises franchises) {
        return franchisesRepo.save(franchises);
    }

    @Override
    public void deleteById(Integer id) {
        franchisesRepo.deleteById(id);
    }

    @Override
    public boolean exists(Integer integer) {
        return false;
    }


}
