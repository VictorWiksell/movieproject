package com.example.movieproject.services.character;


import com.example.movieproject.models.Moviecharacter;
import com.example.movieproject.models.Movies;
import com.example.movieproject.services.CrudService;

import java.util.Collection;

public interface MovieCharacterService extends CrudService<Moviecharacter, Integer> {
    Collection<Moviecharacter> findAllCharactersFromFranchises(int id);

    Collection<Moviecharacter> findCharactersByMovieId(int id);

}
