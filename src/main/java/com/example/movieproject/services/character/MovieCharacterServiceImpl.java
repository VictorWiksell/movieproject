package com.example.movieproject.services.character;


import com.example.movieproject.exceptions.MovieCharacterNotFoundException;
import com.example.movieproject.models.Moviecharacter;
import com.example.movieproject.models.Movies;
import com.example.movieproject.repo.MovieCharacterRepo;
import org.springframework.stereotype.Service;

import java.util.Collection;



@Service
public class MovieCharacterServiceImpl implements MovieCharacterService {


  private final MovieCharacterRepo movieCharacterRepo;

  public MovieCharacterServiceImpl(MovieCharacterRepo movieCharacterRepo) {
    this.movieCharacterRepo = movieCharacterRepo;
  }

  @Override
  public Moviecharacter findById(Integer id) throws MovieCharacterNotFoundException {
    return movieCharacterRepo.findById(id).orElseThrow(
            () -> new MovieCharacterNotFoundException(id));
  }

  @Override
  public Collection<Moviecharacter> findAll() {
    return movieCharacterRepo.findAll();
  }

  @Override
  public Moviecharacter add(Moviecharacter movieCharacter) {
    return  movieCharacterRepo.save(movieCharacter);
  }

  @Override
  public Moviecharacter update(Moviecharacter movieCharacter) {

    return movieCharacterRepo.save(movieCharacter);
  }
  @Override
  public Collection<Moviecharacter> findAllCharactersFromFranchises(int id) {
    return movieCharacterRepo.findAllCharactersFromFranchises(id);
  }

  @Override
  public Collection<Moviecharacter> findCharactersByMovieId(int id) {
    return movieCharacterRepo.findMoviecharacterByMovieId(id);
  }

  @Override
  public void deleteById(Integer id) {
    movieCharacterRepo.deleteById(id);
  }

  @Override
  public boolean exists(Integer integer) {
    return false;
  }










}
