package com.example.movieproject.services.movie;


import com.example.movieproject.exceptions.MovieNotFoundException;
import com.example.movieproject.models.Moviecharacter;
import com.example.movieproject.models.Movies;
import com.example.movieproject.repo.MovieCharacterRepo;
import com.example.movieproject.repo.MovieRepo;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class MovieServiceImpl implements MovieService {


    private final MovieRepo movieRepo;
    private final MovieCharacterRepo characterRepo;

    public MovieServiceImpl(MovieRepo movieRepo, MovieCharacterRepo characterRepo) {
        this.movieRepo = movieRepo;
        this.characterRepo = characterRepo;
    }

    @Override
    public Movies findById(Integer id) {
        return movieRepo.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
    }

    @Override
    public Movies addCharactersToMovie(List<Integer> characterIds, int id) {
        Optional<Movies> movies = movieRepo.findById(id);
        Movies newMovie = movies.get();
        for (int i = 0; i < characterIds.size(); i++) {
            Optional<Moviecharacter> movChar = characterRepo.findById(characterIds.get(i));
            newMovie.addCharacter(movChar.get());
        }
        return movieRepo.save(newMovie);
    }

    @Override
    public Collection<Movies> findAll() {
        return movieRepo.findAll();
    }


    @Override
    public Movies add(Movies movies) {
        return movieRepo.save(movies);
    }

    @Override
    public Movies update(Movies movies) {
         Optional<Movies> newMovie = movieRepo.findById(movies.getId());

        return movieRepo.save(movies);
    }

    @Override
    public Collection<Movies> findAllMoviesByFranchise(int id) {
        return movieRepo.findMoviesByFranchisesId(id);
    }

    @Override
    public void deleteById(Integer id) {
        movieRepo.deleteById(id);

    }

    @Override
    public boolean exists(Integer integer) {
        return false;
    }

}
