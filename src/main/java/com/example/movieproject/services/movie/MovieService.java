package com.example.movieproject.services.movie;


import com.example.movieproject.models.Movies;
import com.example.movieproject.services.CrudService;

import java.util.Collection;
import java.util.List;

public interface MovieService extends CrudService<Movies, Integer> {
    Movies addCharactersToMovie(List<Integer> characterIds, int id);
    Collection findAllMoviesByFranchise(int id);

}
