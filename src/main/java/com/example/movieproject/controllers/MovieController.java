package com.example.movieproject.controllers;



import com.example.movieproject.Dto.MovieDto;
import com.example.movieproject.Dto.MoviecharacterDto;
import com.example.movieproject.mappers.MovieMapper;
import com.example.movieproject.models.Movies;
import com.example.movieproject.services.movie.MovieServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Set;


@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {


    private final MovieServiceImpl movieServiceImpl;
    private final MovieMapper movieMapper;

    public MovieController(MovieServiceImpl movieServiceImpl, MovieMapper movieMapper) {
        this.movieServiceImpl = movieServiceImpl;
        this.movieMapper = movieMapper;
    }


    // Receive a list of movies
    @Operation(summary = "Find all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MoviecharacterDto.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json")})
            // schema = @Schema(implementation = ApiE.class)) })

    })

    @GetMapping()
    public ResponseEntity findAllMovies(){
        Collection<MovieDto> movies = movieMapper.moviesToMovieDto(
                movieServiceImpl.findAll()
        );
        return ResponseEntity.ok(movies);
    }

    // Receive a list of movies in a franchise
    @GetMapping("/franchise/{id}")
    public ResponseEntity findAllMoviesByFranchise(@PathVariable int id){
        Collection<MovieDto> movieDtos = movieMapper.moviesToMovieDto(
                movieServiceImpl.findAllMoviesByFranchise(id)
        );
        return ResponseEntity.ok(movieDtos);
    }


    //Get movie by id
    @Operation(summary = "Find a movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MoviecharacterDto.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json")})
            // schema = @Schema(implementation = ApiE.class)) })

    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        MovieDto movie = movieMapper.moviesToMovieDto(
                movieServiceImpl.findById(id)
        );
        return ResponseEntity.ok(movie);
    }

    //Create new movie, franchise can be added when creating new movie
    @Operation(summary = "Add a new movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json")}),
            //schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @PostMapping()
    public ResponseEntity add(@RequestBody Movies movies){
        Movies newMovie = movieServiceImpl.add(movies);
        URI uri = URI.create("movies/" + newMovie.getId());
        return ResponseEntity.created(uri).build();
    }


    //Update movie, all info will be replaced by the new ones
    @Operation(summary = "Update a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json")}),
            //schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody MovieDto movieDto, @PathVariable int id){
        if(id != movieDto.getId()){
            return ResponseEntity.badRequest().build();
        }
        movieServiceImpl.update(
                movieMapper.movieDtoToMovies(movieDto)
        );
        return ResponseEntity.noContent().build();
    }

    //separate method for adding new characters to movie
    @Operation(summary = "Add characters into a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Characters successfully added into movie",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json")}),
            //schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })

    @PutMapping("/addCharacters/{id}")
    public ResponseEntity addCharactersToMovie(@RequestBody List<Integer> characterIds, @PathVariable int id){
        movieMapper.moviesToMovieDto(movieServiceImpl.addCharactersToMovie(characterIds, id));
        return ResponseEntity.noContent().build();
    }


    //Delete movie
    @Operation(summary = "Delete a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success deleted",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MoviecharacterDto.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json")})

    })


    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable int id){
        movieServiceImpl.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
