package com.example.movieproject.controllers;


import com.example.movieproject.Dto.MovieDto;
import com.example.movieproject.Dto.MoviecharacterDto;
import com.example.movieproject.Dto.MoviecharacterDto;
import com.example.movieproject.mappers.MovieCharacterMapper;
import com.example.movieproject.models.Moviecharacter;
import com.example.movieproject.models.Moviecharacter;
import com.example.movieproject.services.character.MovieCharacterServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;


@RestController
@RequestMapping(path = "api/v1/characters")
public class MovieCharacterController {

    private final MovieCharacterServiceImpl movieCharacterServiceImpl;
    private final MovieCharacterMapper movieCharacterMapper;

    public MovieCharacterController(MovieCharacterServiceImpl movieCharacterServiceImpl, MovieCharacterMapper movieCharacterMapper) {
        this.movieCharacterServiceImpl = movieCharacterServiceImpl;
        this.movieCharacterMapper = movieCharacterMapper;
    }

    //Will return a list of all characters
    @Operation(summary = "Find all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MoviecharacterDto.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json")})

    })
    @GetMapping
    public ResponseEntity findAllCharacters() {

        Collection<MoviecharacterDto> characters = movieCharacterMapper.movieCharacterToMoviecharacterDto(
                movieCharacterServiceImpl.findAll()
        );
        return ResponseEntity.ok(characters);
    }

    //Will return the character with the passed id
    @Operation(summary = "Find a character by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MoviecharacterDto.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json")})

    })
    @GetMapping("{id}")
    public ResponseEntity findCharacterById (@PathVariable int id) {
        MoviecharacterDto character = movieCharacterMapper.movieCharacterToMoviecharacterDto(

                movieCharacterServiceImpl.findById(id)
        );
        return ResponseEntity.ok(character);
    }

    // will return all Characters in a franchise
    @Operation(summary = "Find all characters in a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MoviecharacterDto.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json")})

    })
    @GetMapping("/franchises/{id}")
    public ResponseEntity findCharactersFromFranchises(@PathVariable int id){
        Collection<MoviecharacterDto> movieDtos = movieCharacterMapper.movieCharacterToMoviecharacterDto(
                movieCharacterServiceImpl.findAllCharactersFromFranchises(id)
        );
        return ResponseEntity.ok(movieDtos);
    }

    //Create new Character
    @Operation(summary = "Adding a new character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json")}),
            //schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    @PostMapping
    public  ResponseEntity add(@RequestBody Moviecharacter movieCharacter) {
        Moviecharacter newCharacter = movieCharacterServiceImpl.add(movieCharacter);
        URI location = URI.create("characters/" + newCharacter.getId());
        return ResponseEntity.created(location).build();
    }

    //Update Character, movie cant be added/changed here
    @Operation(summary = "Updating a character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json")}),
            //schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })

    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody MoviecharacterDto movieCharacterDto, @PathVariable int id){
        if(id != movieCharacterDto.getId()){
            return ResponseEntity.badRequest().build();
        }
        movieCharacterServiceImpl.update(
                movieCharacterMapper.movieCharacterDtoToMovieCharacter(movieCharacterDto)
        );
        return ResponseEntity.noContent().build();
    }


    //delete Character
    @Operation(summary = "Deleting a character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success deleted",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MoviecharacterDto.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json")})

    })

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable int id){
        movieCharacterServiceImpl.deleteById(id);
        return ResponseEntity.noContent().build();
    }


}
