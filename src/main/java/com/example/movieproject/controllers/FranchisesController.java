package com.example.movieproject.controllers;

import com.example.movieproject.Dto.FranchiseDto;


import com.example.movieproject.mappers.FranchiseMapper;
import com.example.movieproject.models.Franchises;
import com.example.movieproject.services.franchise.FranchisesServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;



@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchisesController {


        private final FranchiseMapper franchiseMapper;
        private final FranchisesServiceImpl franchisesServiceImpl;

    public FranchisesController(FranchiseMapper franchiseMapper, FranchisesServiceImpl franchisesServiceImpl) {
        this.franchiseMapper = franchiseMapper;
        this.franchisesServiceImpl = franchisesServiceImpl;
    }


    //This method will receive a list of all franchises in DB
    @GetMapping()
    public ResponseEntity getAll() {
        Collection<FranchiseDto> franchiseDtos = franchiseMapper.FranchisesToFranchiseDto(
                franchisesServiceImpl.findAll()
        );
        return ResponseEntity.ok(franchiseDtos);
    }


    //This method will receive the franchise with the id that pass in to the url
    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id) {
        FranchiseDto fran = franchiseMapper.FranchisesToFranchiseDto(
                franchisesServiceImpl.findById(id)
        );
        return ResponseEntity.ok(fran);
    }


    // This method will create a new franchise, you are NOT able to add movies when creating new franchise
    @PostMapping()
    public ResponseEntity add(@RequestBody Franchises franchises){
        Franchises newFran = franchisesServiceImpl.add(franchises);
        URI uri = URI.create("franchises/" + newFran.getId());
        return ResponseEntity.created(uri).build();
    }


    //Update franchise, all old info will be replaced by the new ones except the movies. the relation between franchise and movie will be created through movie url
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody FranchiseDto franchiseDto, @PathVariable int id){
        if(id != franchiseDto.getId()){
            return ResponseEntity.badRequest().build();
        }
        franchisesServiceImpl.update(
                franchiseMapper.FranchiseDtoToFranchises(franchiseDto)
        );
        return ResponseEntity.noContent().build();
    }


    // delete franchise by id. Delete Franchises with added movies will not pass
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable int id){
        franchisesServiceImpl.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}



