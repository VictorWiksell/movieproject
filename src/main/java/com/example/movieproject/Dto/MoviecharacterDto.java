package com.example.movieproject.Dto;


import lombok.Data;

import java.util.Set;

@Data
public class MoviecharacterDto {
    private int id;
    private String alias;
    private String fullName;
    Set<Integer> movies;
}
