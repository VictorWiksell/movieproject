package com.example.movieproject.Dto;


import lombok.Data;

import java.util.Set;

@Data
public class FranchiseDto {
    private int id;
    private String name;
    Set<Integer> movies;
}
