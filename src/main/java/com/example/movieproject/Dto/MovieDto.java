package com.example.movieproject.Dto;


import lombok.Data;
import lombok.Value;

import java.util.List;
import java.util.Set;

@Data
public class MovieDto {

    private int id;
    private String title;
    private String genre;
    private String releaseYear;
    private int franchises;
    Set<Integer> moviecharacter;

}
