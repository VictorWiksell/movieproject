# MovieProject
MovieProject is a spring boot application with postgresql/docker.
This project are setted up with urls where users can add, connect relations and receive movies, characters and franchises. 

## Contributors
@VictorWiksell @hassanyaasin11 @huah1600

## Installation
Git clone this project to get started. We used Java 18 for this application so make sure you have updated JDK.
If you have Docker installed, it will create an image when starting this APP.
unfortunately we didnt succeed connect our datebase on heroku.




## Usage
When using Swagger you will receive url request where you can set up your own movie library. 

## Contributing
Pull requests are welcome
